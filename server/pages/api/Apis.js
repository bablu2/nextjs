module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 18);
/******/ })
/************************************************************************/
/******/ ({

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("X4AT");


/***/ }),

/***/ "X4AT":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("zr5I");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
 //register api that use when user register path where it used /page/[page]/signup.js

function signUp(PostData = {}) {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/signup/", PostData, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} //api for login user.  path where it used /page/[page]/login.js


function LoginUser(PostData = {}) {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/login/", PostData, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} //api for logout user  path where it used /page/_app.js


function logoutApi(PostData) {
  const headers = {
    'Authorization': PostData
  };
  const data = '';
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/logout/", data, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for add product to cart  path where it used /page/product/[name]/[id].js


function addToCart(PostData = {}) {
  const headers = {
    'Authorization': `${PostData.token}`
  };
  const PostDataa = {
    "product_id": PostData.product_id,
    "quantity": PostData.quantity
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/add_to_cart/", PostDataa, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} //api for get banner details and categoried listing according to store , path where it used /page/index.js


function getBanners(PostData = {}) {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("http://commandconcept.csdevhub.com/api/v1/store_banner/", PostData, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for get product by categories  path where it used /page/product/productcategories/[categories]/[id].js


function getProductByCategories(PostData = {}) {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/get_products_by_category/", PostData, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} //api for display cart data  path where it used /page/cart/viewcart.js


function getAllCartProduct(PostData) {
  const headers = {
    'Authorization': PostData
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("http://commandconcept.csdevhub.com/api/v1/get_cart_details/", {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for update qty in cart  path where it used /page/cart/viewcart.js


function updateProductQty(PostData = {}) {
  const headers = {
    'Authorization': PostData
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("http://commandconcept.csdevhub.com/api/v1/get_cart_details/", {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} //api for delete product from cart, path where it used /page/cart/viewcart.js


function deleteProductFromAddToCart(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = {
    "product_id": PostData.product_id
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/delete_cart_product/", PostDataa, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for get list of address of login user , path where it used /page/checkout/addressList.js


function manageAddress(PostData) {
  const headers = {
    'Authorization': PostData
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("http://commandconcept.csdevhub.com/api/v1/manage_address/", {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for save user address path where it used /page/checkout/address.js


function saveAddress(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = {
    "data": PostData.data,
    "operation": PostData.address_type
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/manage_address/", PostDataa, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for delete address ,path where it used /page/checkout/addressList.js


function deleteAddress(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = {
    "data": {
      "address_id": PostData.address_id
    },
    "operation": 'delete'
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/manage_address/", PostDataa, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for get profile related data like persional,address list,order list, path where it used /page/user/profile.js


function getProfilePageData(PostData) {
  const headers = {
    'Authorization': PostData
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("http://commandconcept.csdevhub.com/api/v1/profile_details/", {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for change user login password path where it used /page/user/profile.js


function changeUserPassword(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = {
    "old_password": PostData.old_password,
    "new_password": PostData.new_password
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/change_password/", PostDataa, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for add product to wishlist path where it used /page/product/[name]/[id].js


function addToWishlist(PostData = {}) {
  const headers = {
    'Authorization': `${PostData.token}`
  };
  const PostDataa = {
    "product_id": PostData.product_id
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/add_to_wishlist/", PostDataa, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for show product listing in wishlist path where it used /page/wishlist/wishlist.js


function getAllWishListProduct(PostData) {
  const headers = {
    'Authorization': PostData
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("http://commandconcept.csdevhub.com/api/v1/get_wishlist_details/", {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
} // api for delete product from wish list path where it used /page/wishlist/wishlist.js


function deleteProductFromwishlist(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = {
    "product_id": PostData.product_id
  };
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("http://commandconcept.csdevhub.com/api/v1/delete_wishlist_product/", PostDataa, {
    headers
  }).then(function (response) {
    return response;
  }).catch(error => {
    return error.response;
  });
}

const api = {
  signUp,
  getBanners,
  getProductByCategories,
  LoginUser,
  addToCart,
  logoutApi,
  getAllCartProduct,
  updateProductQty,
  deleteProductFromAddToCart,
  manageAddress,
  saveAddress,
  deleteAddress,
  getProfilePageData,
  changeUserPassword,
  addToWishlist,
  getAllWishListProduct,
  deleteProductFromwishlist
};
/* harmony default export */ __webpack_exports__["default"] = (api);

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });